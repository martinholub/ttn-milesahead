import path from 'path'
import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import HardSourceWebpackPlugin from 'hard-source-webpack-plugin'
import CompressionPlugin from 'compression-webpack-plugin'

const cesiumSource = 'node_modules/cesium/Source'

module.exports = {
  context: __dirname,
  entry: {
    index: './src/index.js'
  },
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, 'public'),
    sourcePrefix: '' // Needed to compile multiline strings in Cesium
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['cache-loader', 'style-loader', 'css-loader']
      }, {
        test: /\.(png|gif|jpg|jpeg|svg|xml|json)$/,
        use: ['cache-loader', 'url-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  amd: {
    toUrlUndefined: true // Enable webpack-friendly use of require in Cesium
  },
  node: {
    fs: 'empty' // Resolve node module use of fs
  },
  resolve: {
    alias: {
      cesium: path.resolve(__dirname, cesiumSource)
    }
  },
  plugins: [
    new HtmlWebpackPlugin({template: 'src/index.html'}),
    new HardSourceWebpackPlugin(),
    new CopyWebpackPlugin([{from: path.join(cesiumSource, '../Build/Cesium/Workers'), to: 'Workers'}]),
    new CopyWebpackPlugin([{from: path.join(cesiumSource, 'Assets'), to: 'Assets'}]),
    new CopyWebpackPlugin([{from: path.join(cesiumSource, 'Widgets'), to: 'Widgets'}]),
    new CopyWebpackPlugin([{from: 'data/', to: 'data/'}]),
    new webpack.DefinePlugin({CESIUM_BASE_URL: JSON.stringify('./')}),
    new CompressionPlugin({})
  ]
}