class ApiClient {
  /**
   * @param {String} baseUrl
   */
  constructor (baseUrl) {
    this._baseUrl = baseUrl
  }

  /**
   * @param {Number} limit
   * @returns {Object[]}
   */
  async getRecords ({limit}) {
    let response = await fetch(`${this._baseUrl}/api/records?limit=${limit}`)
    let records = JSON.parse(await response.text())

    // Cleaning is now done in `UplinkDecoder.js`, but was not for old data
    records.forEach((record) => {
      if (record['lat'] === 0 && record['lon'] === 0) {
        delete record['lat']
        delete record['lon']
      }
      if (record['temp-inside'] === -50) {
        delete record['temp-inside']
      }
      if (record['temp-outside'] === -50) {
        delete record['temp-outside']
      }
    })

    return records
  }
}

export default ApiClient