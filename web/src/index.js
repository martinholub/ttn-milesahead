import 'cesium/Widgets/widgets.css'
import './css/index.css'
import CesiumGlobe from './CesiumGlobe'
import ApiClient from './ApiClient'

async function main () {
  let apiClient = new ApiClient('https://ttn-milesahead.herokuapp.com')
  let cesiumGlobe = new CesiumGlobe()
  cesiumGlobe.setup('cesiumContainer')

  let records = await apiClient.getRecords({limit: 1000})
  records = records.filter((record) => {
    return ('lat' in record) && ('lon' in record) && ('temp-inside' in record)
  })

  let valueList = records.map((r) => r['temp-inside'])
  let valueMin = Math.min(...valueList)
  let valueMax = Math.max(...valueList)
  records.forEach(async (record) => {
    let value = transformRange(record['temp-inside'], valueMin, valueMax)
    await cesiumGlobe.addMarker(record['lat'], record['lon'], value)
  })
}

function transformRange (value, inMin, inMax, outMin = 0, outMax = 1) {
  value = (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
  return Math.max(outMin, Math.min(outMax, value))
}

main()
