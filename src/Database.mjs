import MongoClient from 'mongodb'

class Database {

  /**
   * @param {String} mongoUri
   * @param logger
   */
  constructor (mongoUri, logger) {
    this._logger = logger
    this._mongoUri = mongoUri
  }

  async addRecord (deviceId, data) {
    let collection = await this._getMongoCollection()
    let document = {
      'deviceId': deviceId,
      'receivedAt': new Date(),
      ...data,
    }
    collection.insert(document)
  }

  /**
   * @param {Number} limit
   * @returns {Object[]}
   */
  async getRecords ({limit}) {
    let collection = await this._getMongoCollection()
    let cursor = collection.find().sort({_id: -1})
    if (limit) {
      cursor.limit(limit)
    }
    return cursor.toArray()
  }

  /**
   * @returns {MongoClient}
   * @private
   */
  async _getMongoDb () {
    if (!this._mongoClient) {
      this._mongoClient = await MongoClient.connect(this._mongoUri, {
        useNewUrlParser: true,
      })
    }
    return this._mongoClient
  }

  /**
   *
   * @returns {Collection}
   * @private
   */
  async _getMongoCollection () {
    if (!this._mongoCollection) {
      let client = await MongoClient.connect(this._mongoUri, {
        useNewUrlParser: true,
      })
      this._mongoCollection = client.db().collection('record')
      await this._mongoCollection.createIndex({
        'receivedAt': 1,
      })
    }
    return this._mongoCollection
  }

}

export default Database
