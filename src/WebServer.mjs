import express from 'express'
import bodyParser from 'body-parser'

class WebServer {

  /**
   * @param {Number} port
   * @param {Database} db
   * @param logger
   */
  constructor (port, db, logger) {
    this._port = port
    this._db = db
    this._logger = logger
  }

  async run () {
    let router = express.Router({})
    router.use(bodyParser.urlencoded({extended: true}))
    router.use(bodyParser.json())

    router.get('/records', async (req, res) => {
      let limit = parseInt(req.query['limit'])
      let records = await this._db.getRecords({
        limit: limit,
      })
      res.set('Access-Control-Allow-Origin', '*')
      res.status(200).send(records)
    })

    let app = express()
    app.use('/api', router)
    app.use('/', express.static('web/public/'))

    app.listen(this._port, () => {
      this._logger.info(`Web server listening on port "${this._port}".`)
    })
  }
}

export default WebServer
