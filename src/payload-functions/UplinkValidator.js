function Validator (converted, port) {
  if (port === 1) {
    if (
      (typeof converted['lat'] === 'number') &&
      (typeof converted['lon'] === 'number') &&
      (typeof converted['temp-inside'] === 'number') &&
      (typeof converted['temp-outside'] === 'number')
    ) {
      return true
    }
  }
  return false
}
